let tinhDTB=(...scoreArray) => { 
    let sum=0;
    scoreArray.forEach(item=> {
        sum=sum+item;
    });
    return(sum/scoreArray.length);    
};

let averageScore1=()=>{
    let hoa=document.getElementById('inpHoa').value*1;
    let toan=document.getElementById('inpToan').value*1;
    let ly=document.getElementById('inpLy').value*1;
    document.getElementById('tbKhoi1').innerHTML=tinhDTB(toan,ly,hoa).toPrecision(3);
};
let averageScore2=()=>{
    let van=document.getElementById('inpVan').value*1;
    let su=document.getElementById('inpSu').value*1;
    let dia=document.getElementById('inpDia').value*1;
    let english=document.getElementById('inpEnglish').value*1;
    document.getElementById('tbKhoi2').innerHTML=tinhDTB(van,su,dia,english).toPrecision(3);
};


